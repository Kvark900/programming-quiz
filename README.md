# Mobilne Aplikacije
## Projekt 1 - Kviz aplikacija za predmet Programiranje I
### Student: Žigović Kemal

### Opis rada aplikacije
Aplikacija je namijenjena za provjeru znanja putem kviz pitanja iz osnovnih koncepata programiranja, te programskog jezika Python.  

Kviz sadržava pitanja različitog tipa i težine. Tipovi su:
* pitanja sa višestrukim odgovorom (jedan tačan ili više tačnih) i
* unos odgovora (nekoliko riječi).


Pitanja su kategorisana u tri nivoa težine: 
* lagano, 
* srednje i 
* teško. 

Prije početka kviza korisnik bira težinu kviza i broj pitanja. 

![alt text](https://i.ibb.co/NCc27CB/level-ss.png)

![alt text](https://i.ibb.co/k4bPSv0/num-Of-Questions-ss.png)

Odabrana težina određuje distribuciju pitanja po težini.  Kviz će uključivati minimum 50% pitanja odabrane težine, te po 25% drugih težina.

Prilikom odgovaranja na pitanje korisnik ima mogućnost na “džoker” pitaj prijatelja, koji može ostvariti tako što će uputiti poziv prijatelju ili poslati poruku sa sadržajem pitanja i ponuđenim odgovorima odnosno bez odgovora ukoliko je takav tip pitanja. Ovaj “džoker” može iskoristiti samo jednom tokom kviza.

![alt text](https://i.ibb.co/W6s3FRQ/game-ss.png)

Po okončanju kviza korisniku se prikazuje broj i procenat ispravnih odgovora. Također mu se nudi opcija ponovnog igranja kivza, kao i mogućnost dijeljenja rezultata putem “share” menija.

![alt text](https://i.ibb.co/x2hFsW2/results-ss.png)

### Opis arhitekture aplikacije
Aplikacija slijedi standardni android raspored foldera. U java folderu nalazi se paket `ba.unsa.pmf` unutar kojeg su raspoređeni fragmenti i view modeli prema aplikacijskim segmentima. Postoje četiri osnovna segment paketa: 
*	game
*	result
*	service
*	settings  

`Game` paket čuva logiku igre u `GameFragment` i `GameViewModel`. `Result paket` čuva i prezentira podatke o rezultatu igre, broj i postotak tačnih odgovora. Settings paket ima dva fragmenta `GameLevelFragment` i `NumberOfQuestionsFragment` koji preko zajedničkog view modela `GameSettingsViewModel` prezentiraju i spašavaju potrebne informacije o postavkama igre: težini kviza i broju pitanja.  
  
Prezentacijski sloj se nalazi u folderu `res`. Svakom fragmentu korespondira jedan `layout` XML dokument u kojem je definisan sam izgled fragmenta. Aplikacija je optimizovana da radi uredno na obje orjentacije ekrana – vertikalno i horizontalno –  na način da su kreirani za svaki fragment posebni izgledi (definisani u XML dokumentima, `layout` i `layout-land` folderima) za obje orjentacije ekrana. 
  
`Menu` folder sadrži pomoćne komponente, tipa trake s alatima i menu trake. `Game_menu` je traka koja koja prikazuje naslov aplikacije i opciju joker pozovi prijatelja, `navdrawer_menu` je bočna traka s dodatnim linkovima, dok je `results_menu` traka koju prikazuje `GameResultFragment` i sadrži opciju podijeli rezultat sa drugima.


Dodatno, applikacija je lokalizovna na Bosanski jezik, dok je defaultni jezik Engleski. Sva pitanja i odgovori, labele i drugi stringovi su lokalizovani u folderu `values/strings`. 


Aplikacija slijedi princip **separation of concerns**. Logika koja opslužuje UI dio nalazi se isključivo u fragment klasama, dok dio koji upravlja podacima i nudi ih fragmentima se nalazi u `ViewModel` klasama. 


`ViewModel` klase služe za spašavanje state-a aplikacije uzimajući u obzir i životni ciklus fragmenta. Fragmenti imaju potrebne `ViewModel` reference kojima dobavljaju potrebne podatke za prezentacijski sloj. `ViewModel` klase omogućuju da podaci budu sačuvani i nakon konfiguracijskih promjena kao što su rotacija ekrana i sl.


U aplikaciji je korišten View Binding koji ima znatne prednosti u odnosu na findViewById funkcionalnost.


### Opis funkcionalnosti pojedinačnih klasa
#### <ins>Klasa MainActivity</ins>

Osnovna klasa koja u svom layoutu definiše NavHostFragment i navigacijski graf navigation.xml. U ovom dokumentu grafički je prikazana shema navigacije koja počinje od fragmenta u kojem korisnik bira težinu pitanja. Navigiranjem naprijed, klikom na dugme next, otvara se fragment `numberOfQuestionsFragment`. Ovdje korisnik bira broj pitanja, a klikom na dugme igraj pokreće pitanja na fragmentu `gameFragment`. Nakon završetka igre otvrara se fragment `gameResultFragment `koji sumira rezultate, te prikazuje broj i procenat tačnih odgovora, kao i informaciju da li je iskorišten džoker. Korisnik može birati da igra ponovo.
#### <ins>Klasa `GameFragment `</ins>
Ključni fragment koji sadrži logiku igre, prezentira odgovarajuća pitanja te provjerava tačnost odgovora.
```java 
fun onCreateView(inflater: LayoutInflater,
                 container: ViewGroup?,
                 savedInstanceState: Bundle?): View? 
```

Metoda životnog ciklusa fragmenta koja se poziva prilikom kreiranja prikaza fragmenta. Ovdje se inicijaliziraju i uslovno renderiraju pitanja. Ukoliko je pitanje otvorenog tipa renderira se `EditText  `komponenta, u suprotnom (kada je pitanje tipa višestruki odgovor) renderiraju se `CheckBox`-ovi. Na klik podnesi dugmeta inicijalizira sa event listener koji zapravo uključuje logiku provjere tačnosti odgovora i prelaska na naredno pitanje.


Logika provjere tačnosti odgovora na pitanja višestrukog izbora uključuje jednostavnu provjeru da li je simetrična razlika tačnih odgovora na trenutno pitanja i odabranih odgvora zapravo prazan skup.


S druge strane provjera ispravnosti otvorenih odgovora podrazumjeva pretragu podnesenog ogovora u ponuđenim tačnim odgovorima, zanemarujući velika i mala slova.
#### <ins>Klasa `GameViewModel`</ins>
`ViewModel` klasa koja je zadužena za čuvanje podataka čak i nakon konfiguracijskih promjena kao što su rotacija ekrana i sl.


Sva pitanja se čuvaju u `MutableList`-i. Struktura pitanja je definisana u podatkovnoj klasi `Question`. Pitanje ima atribute kao što su tekst, tip, težina i odgovore. Odgovori na pitanja se čuvaju u `MutableMap`, gdje je ključ tekst odgovora, a vrijednost boolean flag koji označava da li je odgovor tačan ili pogrešan. Ovo je korisno kod pitanja sa više ponuđenih odgovora, dok kod otvorenih pitanja ovo omogućuje  podršku za više načina odgovora također. 


U ovoj klasi je definisana distribucija pitanja na bazi odabrane težine. Kviz će uključivati minimum 50% pitanja odabrane težine, te po 25% drugih težina.

### Opis opštih koncepata Android frameworka<sup><sup name="a1">[1](#resources)</sup>
#### Aktivnost

Aktivnost je jedinstvena i usredotočena stvar koju korisnik može učiniti. Gotovo sve aktivnosti su u interakciji s korisnikom, pa se klasa `Activity` brine za to da stvori prozor za u koji se može postaviti UI pomoću `setContentView(View)`. Iako se aktivnosti korisniku često prikazuju kao prozori na cijelom ekranu, one se mogu koristiti i na druge načine: kao plutajući prozori (preko teme s skupom `R.attr.windowIsFloating`), u načinu rada s više prozora ili ugrađeni u druge prozore. Postoje dvije metode koje će gotovo sve podrazrede aktivnosti primijeniti:
*	`onCreate(Bundle)` je mjesto na kojem se inicijaliziraju aktivnost. Najvažnije je da se ovdje obično poziva  setContentView(int) s resursom izgleda koji definira korisničko sučelje i pomoću findViewById(int)  dobavljaju widgeti s kojima se komunicira programski.
*	`onPause()` je mjesto gdje radite s korisnikom pauzirate aktivnu interakciju s aktivnošću. Sve promjene koje korisnik izvrši u ovom trenutku trebaju se počiniti (obično na ContentProvideru koji drži podatke). U tom je stanju aktivnost i dalje vidljiva na zaslonu.

**Životni ciklus aktivnosti**


Aktivnostima se upravljaja u sistemu putem stack strukture (activity stack). Kada se pokrene nova aktivnost, ona se obično postavlja na vrh trenutnog stacka i postaje aktivna aktivnost - prethodna aktivnost uvijek ostaje ispod nje u stacku i neće doći na vrh stacka dok nova aktivnost ne prestane. Na zaslonu može biti jedan ili više stackova aktivnosti.


Aktivnost uglavnom ima četiri stanja:

Ako je neka aktivnost u prvom planu zaslona (na najvišoj poziciji stacka), ona je aktivna ili pokrenuta. To je obično aktivnost s kojom korisnik trenutno komunicira.

Ako je neka aktivnost izgubila fokus, ali je i dalje predstavljena korisniku, ona je vidljiva. 

Ako je neka aktivnost u potpunosti prekrivena drugom aktivnošću, ona je zaustavljena ili skrivena. Još uvijek zadržava sve podatke o stanju, međutim, korisniku više nije vidljiva, pa je njen prozor skriven te će često, u slučaju potrebe, njena memorija biti oslobođena.

Sistem može osloboditi aktivnost iz memorije ukidanje procesa čineći je uništenom. Kada se ponovo prikaže korisniku, mora se ponovno pokrenuti i vratiti u prethodno stanje.

Sljedeći dijagram prikazuje važne puteve stanja aktivnosti. Kvadrati predstavljaju callback metode koje se mogu implementirati za izvođenje operacija kada se aktivnost kreće između stanja. Ovali u boji su glavna stanja u kojima aktivnost može biti.

![alt text](https://developer.android.com/images/activity_lifecycle.png)

 
Postoje tri ključne petlje u okviru aktivnosti:
*	Cijeli životni vijek aktivnosti se događa između prvog poziva `onCreate(Bundle)` do konačnog poziva `onDestroy()`. Aktivnost će obaviti sve postavke "globalnog" stanja u `onCreate()`, i osloboditi sve preostale resurse u `onDestroy()`. Na primjer, ako postoji thread pokrenut u pozadini za preuzimanje podataka s mreže, on može kreirati taj thread u `onCreate()`, a zatim ga zaustaviti u `onDestroy()`.
*	Vidljivi vijek trajanja aktivnosti događa se između poziva `onStart()` do odgovarajućeg poziva `onStop()`. Za to vrijeme korisnik može vidjeti aktivnost na zaslonu, iako aktivnost možda nije u prvom planu i u interakciji s korisnikom. Između ove dvije metode mogu se održavati resursi koji su potrebni da bi se korisniku prikazala aktivnost. Na primjer, može se registrirati BroadcastReceiver u `onStart()` da bi se nadgledale promjene koje utječu na korisničko sučelje i odjaviti u `onStop()` kad korisnik više ne vidi šta se prikazuje. Metode `onStart()` i `onStop()` mogu se pozvati više puta kako aktivnost postaje vidljiva i skrivena za korisnika.
*	Vijek trajanja aktivnosti u prvom planu događa se između poziva `onResume()` do odgovarajućeg poziva `onPause()`. Za to vrijeme je aktivnost vidljiva, aktivna i u interakciji s korisnikom. Aktivnost često može prelaziti između nastavljenih i pauziranih stanja - na primjer, kada uređaj spava, kada se isporučuje rezultat aktivnosti, kada se isporučuje novi intent. Stoga bi kod unutar ovih metoda trebao biti prilično jednostavan.

Cijeli životni vijek aktivnosti je definisan u okviru sljedećih metoda:

```java
public class Activity extends ApplicationContext {
     protected void onCreate(Bundle savedInstanceState);

     protected void onStart();

     protected void onRestart();

     protected void onResume();

     protected void onPause();

     protected void onStop();

     protected void onDestroy();
 }
```

#### Fragment  
Fragment predstavlja ponašanje ili dio korisničkog sučelja u `FragmentActivity`. Može se kombinirati više fragmenata u jednoj aktivnosti da bi se kreiralo korisničko sučelje s više panela ili fragment iskoristio u više aktivnosti. Fragment je zapravo modularni dio aktivnosti koji ima vlastiti životni ciklus, prima vlastite ulazne evente i koji se može dodavati ili uklanjati dok je aktivnost u toku.

Fragment mora uvijek biti smješten u aktivnosti, a na životni ciklus fragmenta direktno utječe životni ciklus njegove aktivnosti. Na primjer, kada je aktivnost pauzirana, pauzirani su i svi fragmenti u njoj, ili kada je aktivnost uništena, svi njeni  fragmenti su također uništeni. Međutim, dok je neka aktivnost u toku (nalazi se u resumed stanju životnog ciklusa), može se  manipulirati svakim fragmentom neovisno, poput dodavanja ili uklanjanja istih. Kad se izvodi takva fragmentna transakcija, može se također dodati i u stack kojim upravlja aktivnost - svaki element stacka u aktivnost je zapis transakcije fragmenta koja se dogodila.

Android je predstavio fragmente u Android 3.0 (razina API-ja 11), prije svega radi podrške dinamičnijim i fleksibilnijim dizajnom na velikim ekranima, poput tableta. Budući da je ekran tableta mnogo veći od onog na mobilnom telefonu, postoji više prostora za kombiniranje i razmjenu UI komponenti. Fragmenti omogućuju takav dizajn bez potrebe za upravljanjem složenim promjenama hijerarhije prikaza. Podjelom izgleda aktivnosti na fragmente otvara se mogućnost mijenjanja izgleda aktivnosti tijekom izvođenja i čuvanja tih promjena u back stacku aktivnosti.

Iako je Fragment implementiran kao objekt koji je neovisan od `FragmentActivity` i može se upotrijebiti u više različitih aktivnosti, fragment instanca direktno je vezana za njegovu aktivnost.

Konkretno, fragment može pristupiti instanci `FragmentActivity` pomoću getActivity() i lako izvršavati zadatke poput pronalaska pogleda u izgledu aktivnosti:

`val listView: View? = activity?.findViewById(R.id.list)`

Isto tako, aktivnost može pozvati metode u fragmentu dobavljanjem reference na fragment iz FragmentManager-a, koristeći findFragmentById() ili findFragmentByTag(). Na primjer:

`val fragment = supportFragmentManager.findFragmentById(R.id.example_fragment) as ExampleFragment`

Upravljanje životnim ciklusom fragmenta  je poput upravljanja životnim ciklusom aktivnosti. Kao i neka aktivnost, fragment može postojati u tri stanja:

**_Resumed_**  
Fragment je vidljiv u tekućoj aktivnosti.

**_Pauziran_**  
Druga aktivnost je u fokusu, ali aktivnost u kojoj ovaj fragment živi i dalje je vidljiva (aktivnost u fokusu djelomično je prozirna ili ne pokriva cijeli zaslon).

**_Zaustavljen_**  
Fragment nije vidljiv. Ili je aktivnost zaustavljena ili je fragment uklonjen iz aktivnosti, ali je dodan u back stack. Zaustavljeni fragment je još živ (sistem zadržava sve podatke o stanju). Međutim, nije više vidljiv korisniku i biva uništen ukoliko se aktivnost uništi.

![alt text](https://developer.android.com/images/fragment_lifecycle.png)

Osnovne metode životnog ciklusa koji se pozivaju da dovedu fragment u resumed stanje (u interakciji s korisnikom) su:
* `onAttach(Activity)` pozvana nakon što je fragment povezan s njegovom aktivnošću.
* `onCreate(Bundle)` pozvana da napravi početno stvaranje fragmenta.
* `onCreateView(LayoutInflater, ViewGroup, Bundle)` stvara i vraća hijerarhiju prikaza povezanu s fragmentom.
* `onActivityCreate(Bundle)` govori fragmentu da je njegova aktivnost dovršila vlastitu Activity#onCreate.
* `onViewStateRestored(Bundle)` govori fragmentu da je vraćeno stanje hijerarhije prikaza.
* `onStart()` čini fragment vidljivim za korisnika 
* `onResume()` čini da fragment započne interakciju s korisnikom

Kako se fragment više ne koristi, prolazi kroz obrnutu seriju povratnih poziva:
* `onPause()` fragment više ne komunicira s korisnikom, jer je njegova aktivnost pauzirana 
* `onStop()` fragment više nije vidljiv korisniku jer je njegova aktivnost zaustavljena 
* `onDestroyView()` omogućuje da fragment očisti resurse povezane s njegovim `Viewom`.
* `onDestroy()` uradi konačno čišćenje stanja fragmenta.
* `onDetach()` pozvan neposredno prije nego što fragment više nije povezan s njegovom aktivnošću.


<hr></hr>
<a name="resources">1</a>  

https://developer.android.com/guide/components/fragments  
https://developer.android.com/reference/android/app/Activity  [↩](#a1)

